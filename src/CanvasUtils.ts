import { Poly } from './types';

/**
 * Clip a canvas for a pane via a {@link Poly}.
 */
export function clipViaShape(
  canvasContext: CanvasRenderingContext2D,
  path: Poly
) {
  /// make the path
  canvasContext.beginPath();
  canvasContext.moveTo(path[0].x, path[0].y);
  for (var i = 1; i < path.length; i++) {
    canvasContext.lineTo(
      path[i].x,
      path[i].y,
    );
  }

  canvasContext.closePath();
  canvasContext.clip();
}