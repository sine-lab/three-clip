//
import { RefObject } from 'react';
//
import * as MathPolygon2D from './@vb8/math/MathPoly';
import { Poly } from './@vb8/math/types';
import { clipViaShape } from './CanvasUtils';

/**
 * Render a view.
 */
export function renderPane(props: {
  scene: THREE.Scene,
  renderer: THREE.WebGLRenderer,
  paneCameraRef: RefObject<THREE.Camera>,
  paneCanvasRef: RefObject<HTMLCanvasElement>,
  paneShape: Poly,
  paneShapeScale: number
}): void {
  props.renderer.render( // render scene via pane's camera 
    props.scene,
    props.paneCameraRef.current!
  );

  // update aspect ratio
  if ((props.paneCameraRef.current! as THREE.PerspectiveCamera).isPerspectiveCamera) {
    (props.paneCameraRef.current! as THREE.PerspectiveCamera).aspect = props.renderer.domElement.clientWidth! / props.renderer.domElement.clientHeight!;
  }
  else {
    throw `NotImplemented: Only THREE.PerspectiveCamera is implemented.`
  }

  // determine *normalized* bounds
  let boundsXMin = Number.POSITIVE_INFINITY;
  let boundsXMax = Number.NEGATIVE_INFINITY;
  let boundsYMin = Number.POSITIVE_INFINITY;
  let boundsYMax = Number.NEGATIVE_INFINITY;
  for (let iViewPathIndex = 0; iViewPathIndex < props.paneShape.length; iViewPathIndex++) {
    const iPoint = props.paneShape[iViewPathIndex];
    if (iPoint.x < boundsXMin) boundsXMin = iPoint.x;
    if (iPoint.x > boundsXMax) boundsXMax = iPoint.x;
    if (iPoint.y < boundsYMin) boundsYMin = iPoint.y;
    if (iPoint.y > boundsYMax) boundsYMax = iPoint.y;
  }

  const paneCanvasContext = props.paneCanvasRef.current!.getContext('2d')!; // get pane's rendering context

  paneCanvasContext.save(); // save pre-clip context

  const scaleX = (props.paneCanvasRef.current!.width / props.renderer.domElement.width);
  const scaleY = (props.paneCanvasRef.current!.height / props.renderer.domElement.height);

  const paddedShape = MathPolygon2D.addAlongVertexNormals(props.paneShape.map(i => ({ x: i.x, y: i.y })), props.paneShapeScale); // create a new padded version of the shape
  clipViaShape( // clip pane
    paneCanvasContext,
    paddedShape
  );

  const sw = (boundsXMax - boundsXMin) / scaleX;
  const sh = (boundsYMax - boundsYMin) / scaleY;
  const sx = (props.renderer.domElement.width - sw) / 2;
  const sy = (props.renderer.domElement.height - sh) / 2;

  const dw = (boundsXMax - boundsXMin); // bounds are in destination-space
  const dh = (boundsYMax - boundsYMin);
  const dx = (boundsXMin);
  const dy = (boundsYMin);

  //console.log(`${sx} ${sy} ${sw} ${sh} -> ${dx} ${dy} ${dw} ${dh}`);

  paneCanvasContext?.drawImage( // copy render from renderer to pane
    props.renderer.domElement,
    sx,
    sy,
    sw,
    sh,
    dx,
    dy,
    dw,
    dh,
  );

  paneCanvasContext.restore(); // restore pre-clipping context
}