//
import { Point } from './types';

/**
 * Determine a Y position on a line segment using X position. 
 */
export function lineSegmentSolveY(pointX: number, lineSegmentStart: Point, lineSegmentEnd: Point): number {
  const slope = (lineSegmentEnd.y - lineSegmentStart.y) / (lineSegmentEnd.x - lineSegmentStart.x);

  return slope * (pointX - lineSegmentStart.x) + lineSegmentStart.y;
}

/**
 * Determine a X position on a line segment using Y position. 
 */
export function lineSegmentSolveX(pointY: number, lineSegmentStart: Point, lineSegmentEnd: Point): number {
  const slope = (lineSegmentEnd.y - lineSegmentStart.y) / (lineSegmentEnd.x - lineSegmentStart.x);

  return pointY = lineSegmentStart.x + (pointY - lineSegmentStart.y) / slope;
}