//
import React from 'react';
import { useEffect, useRef, useState } from 'react';
import { RootState, useFrame } from '@react-three/fiber';
import { PerspectiveCamera } from '@react-three/drei';
import equal from 'fast-deep-equal';
//
import * as MathPoly from './@vb8/math/MathPoly';
//
import { PaneShapes, PaneOffsets } from './types';
import { PaneLayouts, layouts } from './const';
import { renderPane } from './Pane';
import { getPanesOfLayout } from './DialogUtils';

/**
 * Renders dialog panes onto {@param props.canvasRef}
 * @param props.canvasRef canvas to render panes onto.
 * @param props.offsetsIn target offsets of all panes.
 */
export default React.memo((props: {
  canvasRef: React.RefObject<HTMLCanvasElement>,
  layoutIn: PaneLayouts,
  offsetsIn: PaneOffsets,
}): JSX.Element => {
  //console.log(`${JSON.stringify(props.offsetsIn)}`)

  // /**
  //  * nb, layout does not include margins. shapes do include margins.
  //  */
  // const margin = 4;

  /**
   * Duration of time for switching between two layouts. ie, the amount of time that panes's shape changes.
   */
  const layoutTransitionDuration = 200;

  const [stateLayoutPrevious, setLayoutPrevious] = useState<PaneLayouts | undefined>();
  const [stateLayoutIn, setLayoutIn] = useState<PaneLayouts>(props.layoutIn);
  const [stateLayoutTransitionEpoch, setLayoutTransitionEpoch] = useState<number | undefined>(undefined);
  const [stateLayoutTransitionTime, setLayoutTransitionTime] = useState<number | undefined>(undefined);

  /**
   * Duration of time for switching between 
   */
  const offsetTranstionDuration = 200;

  const [stateOffsetPrevious, setOffsetPrevious] = useState<PaneOffsets | undefined>();
  const [stateOffsetIn, setOffsetIn] = useState<PaneOffsets | undefined>();
  const [stateOffsetsTransitionTime, setOffsetsTransitionTime] = useState<number | undefined>(undefined);
  const [stateOffsetsTransitionEpoch, setOffsetsTransitionEpoch] = useState<number | undefined>(undefined);

  const refCenterCamera = useRef<THREE.PerspectiveCamera>(null);
  const refRightCamera = useRef<THREE.PerspectiveCamera>(null);
  const refLeftCamera = useRef<THREE.PerspectiveCamera>(null);

  let paneShapes: PaneShapes = getPanesOfLayout(
    layouts[stateLayoutIn],
    props.canvasRef.current!
  );

  // modify pane shapes for layout interpolation
  if (
    stateLayoutPrevious !== undefined &&
    stateLayoutTransitionTime !== undefined
  ) {
    const paneShapeTransitionInterpolant = (stateLayoutTransitionTime ?? 0) / layoutTransitionDuration;

    if (paneShapeTransitionInterpolant < 1) {
      const paneShapesPrevious: PaneShapes = getPanesOfLayout( // get pane layout shapes
        layouts[stateLayoutPrevious],
        props.canvasRef.current!
      );

      const paneShapesIn: PaneShapes = getPanesOfLayout( // get pane layout shapes
        layouts[stateLayoutIn],
        props.canvasRef.current!
      );

      paneShapes = {
        center: MathPoly.lerp(paneShapesPrevious.center, paneShapesIn.center, paneShapeTransitionInterpolant),
        left: MathPoly.lerp(paneShapesPrevious.left, paneShapesIn.left, paneShapeTransitionInterpolant),
        right: MathPoly.lerp(paneShapesPrevious.right, paneShapesIn.right, paneShapeTransitionInterpolant),
      }
    }
  }

  const paneShapesOffsetDestination: PaneShapes = { // determine target pane position
    center: MathPoly.addDirection(paneShapes.center, props.offsetsIn.center),
    left: MathPoly.addDirection(paneShapes.left, props.offsetsIn.left),
    right: MathPoly.addDirection(paneShapes.right, props.offsetsIn.right)
  }

  const paneOffsetsTransitionInterpolant: number = (stateOffsetsTransitionTime ?? 0) / offsetTranstionDuration;

  // determine pane shapes for offset interpolation
  if ( // offsets not transitioning, or completed transition
    stateOffsetsTransitionTime === undefined ||
    stateOffsetPrevious === undefined ||
    paneOffsetsTransitionInterpolant >= 1
  ) {
    paneShapes = paneShapesOffsetDestination; // not transitioning offset
  }
  else { // offset transitioning
    const paneShapesOffsetSource: PaneShapes = { // determine target pane position
      center: MathPoly.addDirection(paneShapes.center, stateOffsetPrevious.center),
      left: MathPoly.addDirection(paneShapes.left, stateOffsetPrevious.left),
      right: MathPoly.addDirection(paneShapes.right, stateOffsetPrevious.right)
    }

    console.log(`${paneOffsetsTransitionInterpolant}:\nsource: ${JSON.stringify(paneShapesOffsetSource)}\ntarget: ${JSON.stringify(paneShapesOffsetDestination)}`);

    paneShapes = {
      center: MathPoly.lerp(paneShapesOffsetSource.center, paneShapesOffsetDestination.center, paneOffsetsTransitionInterpolant),
      left: MathPoly.lerp(paneShapesOffsetSource.left, paneShapesOffsetDestination.left, paneOffsetsTransitionInterpolant),
      right: MathPoly.lerp(paneShapesOffsetSource.right, paneShapesOffsetDestination.right, paneOffsetsTransitionInterpolant),
    }
  }

  useEffect(() => {
    setLayoutPrevious(stateLayoutIn);
    setLayoutIn(props.layoutIn);
    setLayoutTransitionTime(0);
    setLayoutTransitionEpoch(Date.now());

    //console.log(`${PaneLayouts[stateLayoutIn]} -> ${PaneLayouts[props.layoutIn]}`)
  }, [props.layoutIn])

  useEffect(() => {
    console.log(`props.offsetsIn: ${JSON.stringify(props.offsetsIn)}\nstateOffsetIn ${JSON.stringify(stateOffsetIn)}`);

    setOffsetPrevious(stateOffsetIn);
    setOffsetIn(props.offsetsIn);
    setOffsetsTransitionTime(0);
    setOffsetsTransitionEpoch(Date.now());
  }, [props.offsetsIn])


  useFrame(() => {
    // TODO: check time, then finish transitions entering

    // set transition time
    if (stateLayoutTransitionEpoch !== undefined)
      setLayoutTransitionTime(Date.now() - stateLayoutTransitionEpoch);

    // set transition time
    if (stateOffsetsTransitionEpoch !== undefined)
      setOffsetsTransitionTime(Date.now() - stateOffsetsTransitionEpoch);
  })

  useFrame((state: RootState,) => {
    refRightCamera.current?.lookAt(0, 0, 10); // look at protag
    refLeftCamera.current?.lookAt(-10, 0, 10); // look at sidekick
    refCenterCamera.current?.lookAt(0, 0, -10); // look at rival

    props.canvasRef.current?.getContext('2d')?.clearRect(0, 0, props.canvasRef.current.width, props.canvasRef.current.height);

    //console.log(`center: ${ JSON.stringify(paneShapes.center) }, left: ${ JSON.stringify(paneShapes.left) }, right: ${ JSON.stringify(paneShapes.right) }`)

    renderPane({
      scene: state.scene,
      renderer: state.gl,
      paneCanvasRef: props.canvasRef,
      paneCameraRef: refLeftCamera,
      paneShape: paneShapes.left,
      paneShapeScale: -16
    })

    renderPane({
      scene: state.scene,
      renderer: state.gl,
      paneCanvasRef: props.canvasRef,
      paneCameraRef: refCenterCamera,
      paneShape: paneShapes.center,
      paneShapeScale: -16
    })

    renderPane({
      scene: state.scene,
      renderer: state.gl,
      paneCanvasRef: props.canvasRef,
      paneCameraRef: refRightCamera,
      paneShape: paneShapes.right,
      paneShapeScale: -16
    })

    state.gl.render(state.scene, state.camera);
  }, 1);

  return (
    <>
      <PerspectiveCamera ref={refCenterCamera} position={[0, 0, 0]} fov={30} />
      <PerspectiveCamera ref={refLeftCamera} position={[-10, 0, 0]} fov={30} />
      <PerspectiveCamera ref={refRightCamera} position={[0, 0, 0]} fov={30} />

      <ambientLight />
    </>
  );
},
  (prevProps, nextProps) =>
    (prevProps.layoutIn === nextProps.layoutIn && equal(prevProps, nextProps))
);