import { Direction2D, Poly } from './@vb8/math/types';


/**
 * Describes the overall layout of dialog canvas, using normalized coordinates. 
 * Layout does not describe shape position.
 */
export type PaneShapes = {
  left: Poly;

  center: Poly;

  right: Poly;
}

/**
 * Describes the pane offset of dialog canvas, using absolute coordinates.
 */
export type PaneOffsets = {
  left: Direction2D;

  center: Direction2D;

  right: Direction2D;
}