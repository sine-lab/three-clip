//
import { denormalize } from './@vb8/math/MathPoly';
//
import { PaneShapes } from './types';

/**
 * Get individual pane shapes of a layout. Shapes do not include offset nor margins.
 */
export function getPanesOfLayout(
  layout: PaneShapes,
  canvas: HTMLCanvasElement
): PaneShapes {
  return {
    center: denormalize(layout.center, canvas),
    right: denormalize(layout.right, canvas),
    left: denormalize(layout.left, canvas)
  }
}