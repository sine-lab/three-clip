import * as MathFloat32 from './MathFloat32';
import { Direction2D, Point } from './types'

export function add(pointA: Point, pointB: Point): Point {
  return {
    x: pointA.x + pointB.x,
    y: pointA.y + pointB.y
  }
}

export function subtract(minuend: Point, subtrahend: Point): Point {
  return {
    x: minuend.x - subtrahend.x,
    y: minuend.y - subtrahend.y
  }
}

export function lerpComponents(a: Point, b: Point, l: number): Point {
  return {
    x: MathFloat32.lerp(a.x, b.x, l),
    y: MathFloat32.lerp(a.y, b.y, l),
  }
}

export function getLineNormalClockwise(p1: Point, p2: Point): Direction2D {
  return {
    x: -(p2.y - p1.y),
    y: (p2.x - p1.x)
  }
}

export function getLineNormalCounter(p1: Point, p2: Point): Direction2D {
  return {
    x: p2.y - p1.y,
    y: -(p2.x - p1.x)
  }
}