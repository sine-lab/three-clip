export function lerp(a: number, b: number, l: number): number {
  return a + (b - a) * l;
}

export function wrap(i: number, length: number): number {
  return (i % length + length) % length;
}