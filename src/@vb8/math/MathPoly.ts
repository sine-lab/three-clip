//

import * as MathFloat32 from './MathFloat32';
import * as MathPoint2D from './MathPoint2D';
import * as MathDirection2D from './MathDirection2D';
import { Direction2D, Point, Poly } from './types';

/**
 * Interpolate between two shapes by lerping component of each point.
 */
export function lerp(
  paneA: Poly,
  paneB: Poly,
  l: number
): Poly {
  if (paneA.length !== paneB.length)
    throw `InvalidArgument: Polygon shapes must have the same length!`;

  return paneA.map((_, index) => MathPoint2D.lerpComponents(paneA[index], paneB[index], l))
}

/**
 * Denormalize a pane shape from normalized coordinates to actualize cooardinates,
 * @param pane shape of the polygon, in normalized coordinates.
 */
export function denormalize(
  pane: Poly,
  canvas: HTMLCanvasElement
): Poly {
  return pane.map(iPoint => ({
    x: iPoint.x * canvas.width,
    y: iPoint.y * canvas.height
  }))
}

/**
 * Determine position of a vertex a distance from that vertex's normal.
 * @param vertex vertex to add distance along vertex's normal.
 * @param lineAStart start of directed edge which begins at the last vertex, and ends at vertex.
 * @param lineBStart start of directed edge which begins at the next vertex, and ends at vertex.
 */
export function addAlongVertexNormal(
  vertex: Point,
  lineAStart: Point,
  lineBStart: Point,
  addend: number
): Point {
  const lineAVector = MathPoint2D.subtract(vertex, lineAStart) // line A's vector towards intersecting vertex
  const lineBVector = MathPoint2D.subtract(vertex, lineBStart); // line B's vector towards intersecting vertex
  const linesAngle = MathDirection2D.getAngle( // angle between two vectors
    lineAVector,
    lineBVector
  )

  const offsetMagnitude = addend / Math.sin(linesAngle / 2);
  const offsetDirection = MathDirection2D.newNormalize(MathPoint2D.add(
    MathDirection2D.newNormalize(lineAVector),
    MathDirection2D.newNormalize(lineBVector)
  ));
  const offset = MathDirection2D.newDirection2D(
    offsetDirection,
    offsetMagnitude
  );

  return MathPoint2D.add(vertex, offset);
}

/**
 * Create a new polygon with each edge scaled a fixed amount of units.
 * @arg augend a clockwise polygon.
 */
export function addAlongVertexNormals(
  augend: Poly,
  addend: number
): Poly {
  const newPoly: Poly = new Array<Point>(augend.length);
  for (let i = 0; i < augend.length; i++) {
    const iLast = MathFloat32.wrap(i - 1, augend.length);
    const iNext = MathFloat32.wrap(i + 1, augend.length);

    const iPoint = augend[i];
    const lastPoint = augend[iLast];
    const nextPoint = augend[iNext];

    newPoly[i] = addAlongVertexNormal(
      iPoint,
      lastPoint,
      nextPoint,
      addend
    )
  }

  return newPoly;
}

/**
 * Create a polygon whose vertices are the sum of that polygon and another polygon.
 */
export function addPoly(
  augend: Poly,
  addend: Poly
): Poly {
  return augend.map((_, index: number) => ({
    x: augend[index].x + addend[index].x,
    y: augend[index].y + addend[index].y
  }));
}

/**
 * Create a polygon whose vertices are shifted in a direction.
 */
export function addDirection(
  augend: Poly,
  direction: Direction2D
): Poly {
  return augend.map((_, index: number) => ({
    x: augend[index].x + direction.x,
    y: augend[index].y + direction.y
  }));
}