
/**
 * Point within a canvas polygon pane.
 */
export type Point = { x: number, y: number }

/**
 * Not necessarily length of 1.
 */
export type Direction2D = { x: number, y: number }

/**
 * Describes polygon-shaped pane.
 */
export type Poly = Array<Point>;