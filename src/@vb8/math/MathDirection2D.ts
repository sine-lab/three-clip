import { Direction2D } from './types';

export function getLength(direction: Direction2D): number {
  return Math.sqrt(direction.x * direction.x + direction.y * direction.y);
}

export function getLengthSquared(direction: Direction2D): number {
  return direction.x * direction.x + direction.y * direction.y;
}

export function getAngle(from: Direction2D, to: Direction2D): number {
  const denominator: number = Math.sqrt(getLengthSquared(from) * getLengthSquared(to));
  if (denominator === 0)
    return 0;

  const dot = getDot(from, to) / denominator;
  return Math.acos(dot);
}

export function getDot(
  lhs: Direction2D,
  rhs: Direction2D
): number {
  { return lhs.x * rhs.x + lhs.y * rhs.y; }
}

export function newDirection2D(
  direction: Direction2D,
  m: number
): Direction2D {
  return {
    x: direction.x * m,
    y: direction.y * m
  }
}

export function newNormalize(direction: Direction2D): Direction2D {
  const mag: number = getLength(direction);
  if (mag > 0)
    return {
      x: (direction.x / mag),
      y: (direction.y / mag)
    };
  else
    return {
      x: 0,
      y: 0
    };
}