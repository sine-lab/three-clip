import { PaneShapes } from './types';

export enum PaneLayouts {
  AllOrthogonal,

  AllSlantRight,

  AllSlantLeft,
}

/**
 * Layouts do not include styling (ie, padding).
 */
export const layouts: { [key in PaneLayouts]: PaneShapes } = {
  [PaneLayouts.AllOrthogonal]: {
    left: [
      { x: 0, y: 0 },
      { x: 0.2, y: 0 },
      { x: 0.2, y: 1 },
      { x: 0, y: 1 }
    ],

    center: [
      { x: 0.25, y: 0 },
      { x: 0.75, y: 0 },
      { x: 0.75, y: 1 },
      { x: 0.25, y: 1 }
    ],

    right: [
      { x: 0.8, y: 0 },
      { x: 1, y: 0 },
      { x: 1, y: 1 },
      { x: 0.8, y: 1 }
    ]
  },
  [PaneLayouts.AllSlantRight]: {
    left: [
      { x: 0, y: 0.5 }, // tl
      { x: 0.2375, y: 0.25 }, // tr
      { x: 0.2, y: 1 }, // br
      { x: 0, y: 1 } // bl
    ],

    center: [
      { x: 0.25, y: 0 }, // tl
      { x: 0.8, y: 0 }, // tr
      { x: 0.75, y: 1 }, // br
      { x: 0.2, y: 1 } // bl
    ],

    right: [
      { x: 0.8, y: 0.5 },
      { x: 1, y: 0.6 },
      { x: 1, y: 1 },
      { x: 0.75, y: 1 }
    ],
  },
  [PaneLayouts.AllSlantLeft]: {
    right: [
      { x: 0.75, y: 0.6 },
      { x: 1, y: 0.5 },
      { x: 1, y: 1 },
      { x: 0.8, y: 1 }
    ],

    center: [
      { x: 0.2, y: 0 },
      { x: 0.75, y: 0 },
      { x: 0.8, y: 1 },
      { x: 0.25, y: 1 }
    ],

    left: [
      { x: 0, y: 0.6 },
      { x: 0.20, y: 0.5 },
      { x: 0.25, y: 1 },
      { x: 0, y: 1 }
    ]
  }
}