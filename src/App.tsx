//
import { useRef, useState } from "react";
import * as THREE from "three";
import { Canvas } from "@react-three/fiber";
import { Cylinder, OrbitControls } from '@react-three/drei';
//
import Dialog from './Dialog';
import { PaneLayouts } from './const';
import { useKeyState } from 'use-key-state';
//

export default function App() {
  const loader = new THREE.CubeTextureLoader();
  const texture = loader.load([
    "/px.png",
    "/nx.png",
    "/py.png",
    "/ny.png",
    "/pz.png",
    "/nz.png"
  ]);

  const [getLayoutIn, setLayoutIn] = useState<PaneLayouts>(PaneLayouts.AllOrthogonal);
  const [statePaneLeftIn, setPaneLeftIn] = useState<boolean>(true);
  const [statePaneRightIn, setPaneRightIn] = useState<boolean>(true);
  const [statePaneCenterIn, setPaneCenterIn] = useState<boolean>(true);

  const canvasRef = useRef<HTMLCanvasElement>(null);

  const panesToggle = useKeyState({ one: '1', two: '2', three: '3' });
  if (panesToggle.one.down) {
    setPaneLeftIn(!statePaneLeftIn);
    console.log(`toggle 1`);
  }

  if (panesToggle.two.down) {
    setPaneCenterIn(!statePaneCenterIn);
    console.log('toggle 2')
  }

  if (panesToggle.three.down) {
    setPaneRightIn(!statePaneRightIn);
    console.log('toggle 3')
  }

  //console.log(`${statePaneLeftIn} ${statePaneCenterIn} ${statePaneRightIn} `);

  return <>
    <div
      style={{
        position: 'absolute',
        width: 1920,
        height: 1080,
        margin: 'auto',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        imageRendering: 'crisp-edges',
      }}
    >
      <canvas // pane canvas
        ref={canvasRef}
        width={1280}
        height={720}
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%'
        }}
      />

      <Canvas style={{ zIndex: -1 }} onCreated={(state) => state.scene.background = texture}>
        <OrbitControls makeDefault autoRotate />

        <Cylinder args={[0.5, 0.5, 2]} position={[0, 0, 10]}>
          <meshBasicMaterial color={'green'} />
        </Cylinder>

        <Cylinder args={[0.5, 0.5, 2]} position={[-10, 0, 10]}>
          <meshBasicMaterial color={'blue'} />
        </Cylinder>

        <Cylinder args={[0.5, 0.5, 2]} position={[0, 0, -10]}>
          <meshBasicMaterial color={'red'} />
        </Cylinder>

        <Dialog
          layoutIn={getLayoutIn}
          offsetsIn={{
            center: statePaneCenterIn ? { x: 0, y: 0 } : { x: 0, y: -1080 },
            left: statePaneLeftIn ? { x: 0, y: 0 } : { x: -500, y: 0 },
            right: statePaneRightIn ? { x: 0, y: 0 } : { x: 500, y: 0 },
          }}
          canvasRef={canvasRef}
        />
      </Canvas>

      <div style={{
        position: 'absolute',
        height: 256,
        margin: 'auto',
        bottom: 32,
        left: 256,
        right: 256,
        backgroundColor: 'rgba(95, 158, 160, 0.8)',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
      }}>
        <input
          type='button'
          onClick={() => setLayoutIn(PaneLayouts.AllSlantLeft)}
          style={{
            width: 128,
            height: 128
          }}

        />
        <input
          type='button'
          onClick={() => setLayoutIn(PaneLayouts.AllOrthogonal)}
          style={{
            width: 128,
            height: 128
          }}

        />

        <input
          type='button'
          onClick={() => setLayoutIn(PaneLayouts.AllSlantRight)}
          style={{
            width: 128,
            height: 128
          }}

        />
      </div>
    </div>
  </>
}
